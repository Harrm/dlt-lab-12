from erc20 import deploy_contract, compile_source_file, w3
from flask import Flask, render_template, request, Response

app = Flask(__name__)

app.template_folder = '.'


@app.route('/')
def deploy_form():
    return render_template("deploy.html")


@app.route('/do_deploy')
def do_deploy():
    print(request.args)
    try:
        compiled_sol = compile_source_file('erc20_template.sol', request.args['token_name'],
                                           request.args['token_symbol'],
                                           request.args['supply'])
        contract_id, contract_interface = compiled_sol.popitem()

        address = deploy_contract(w3, contract_interface)
    except Exception:
        return render_template("deploy_info.html", text=f"Error deploying contract")

    return render_template("deploy_info.html", text=f"Deployed {address} successfully!")


app.run("127.0.0.1", 4242)
