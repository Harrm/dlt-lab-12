from web3 import Web3
from solcx import compile_standard
import json

w3 = Web3(Web3.WebsocketProvider('wss://ropsten.infura.io/ws/v3/c3d4b4468ae94745aac8ad9a884cc5bf', ))

w3.eth.defaultAccount = "0x5A345852ed35546362e0c1ea66d77C81174f1CC4"

def compile_source_file(file_path, name, symbol, supply):
    with open(file_path, 'r') as f:
        source = f.read()

    source = source.replace("__TOKEN_NAME", name)
    source = source.replace("__TOKEN_SYMBOL", symbol)
    source = source.replace("__TOKEN_SUPPLY", str(supply))
    input = {
        "language": "Solidity",
        "sources": {
            "erc20_template.sol": {
                "content": source
            }
        },
        "settings": {
            "outputSelection": {
                "*": {
                    "*": [
                        "abi", "evm.bytecode"
                    ]
                }
            }
        }
    }
    return compile_standard(input)


def deploy_contract(w3, contract_interface):
    tx_hash = w3.eth.contract(
        abi=contract_interface['abi'],
        bytecode=contract_interface['evm']['bytecode']['object']).constructor().transact()

    address = ""  # w3.eth.getTransactionReceipt(tx_hash)['contractAddress']
    return address


if __name__ == '__main__':
    contract_source_path = 'erc20_template.sol'
    compiled_sol = compile_source_file(contract_source_path, "AAToken", "AAAAA", 100500)
    print(compiled_sol)
    contract_interface = compiled_sol["contracts"][contract_source_path]["AAToken"]
    print(contract_interface)
    address = deploy_contract(w3, contract_interface)
    print(f'Deployed {contract_id} to: {address}\n')
